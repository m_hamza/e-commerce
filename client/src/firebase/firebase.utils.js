import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
	apiKey: 'AIzaSyA3NyQAMptjXvdgIDkNBJGG4_do7wQG_Hw',
	authDomain: 'crwn-db-3cecc.firebaseapp.com',
	databaseURL: 'https://crwn-db-3cecc.firebaseio.com',
	storageBucket: 'crwn-db-3cecc.appspot.com',
	messagingSenderId: '330804436943',
	appId: '1:330804436943:web:be78306112160b01',
	projectId: 'crwn-db-3cecc'
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
	if (!userAuth) {
		console.log('!userAuth');
		return;
	}
	const userRef = firestore.doc(`users/${userAuth.uid}`);
	const snapshot = await userRef.get();
	console.log('firebase util snapshot', snapshot);
	if (!snapshot.exists) {
		const { displayName, email } = userAuth;
		const createdAt = new Date();
		try {
			await userRef.set({
				displayName,
				email,
				createdAt,
				...additionalData
			});
			console.log('afterSet', userRef);
		} catch (error) {
			console.log('error creating user', error.message);
		}
		console.log('Firebase userRef', userRef);
		return userRef;
	}
	return userRef;
};

export const convertCollectionsSnapshotToMap = (collections) => {
	const transformedCollection = collections.docs.map((doc) => {
		const { title, items } = doc.data();
		return {
			routeName: encodeURI(title.toLowerCase()),
			id: doc.id,
			title,
			items
		};
	});
	return transformedCollection.reduce((accumulator, collection) => {
		accumulator[collection.title.toLowerCase()] = collection;
		return accumulator;
	}, {});
};

export const addCollectionsAndDocuments = async (
	collectionKey,
	objectsToAdd
) => {
	const collectionRef = firestore.collection(collectionKey);
	const batch = firestore.batch();
	objectsToAdd.forEach((obj) => {
		const newDocRef = collectionRef.doc();
		batch.set(newDocRef, obj);
	});
	return await batch.commit();
};

firebase.initializeApp(config);
export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const getCurrentUser = () => {
	return new Promise((resolve, reject) => {
		const unsubscribe = auth.onAuthStateChanged((userAuth) => {
			unsubscribe();
			resolve(userAuth);
		}, reject);
	});
};

export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: 'select_account' });
export const SignInWithGoogle = () => auth.signInWithPopup(googleProvider);

export default firebase;
