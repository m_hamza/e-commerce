import React, { useState } from 'react';
import { connect } from 'react-redux';
import './sign-in.styles.scss';
import FormInput from './../form-input/form-input.component';
import CustomButton from '../custom-button/custom-button.component';
import {
	googleSignInStart,
	emailSignInStart
} from '../../redux/user/users.actions';
const SignIn = ({ googleSignInStart, emailSignInStart }) => {
	// constructor(props) {
	// 	super(props);
	// 	this.state = {
	// 		email: '',
	// 		password: ''
	// 	};
	// }

	const [credentials, setCredentials] = useState({ email: '', password: '' });
	const { email, password } = credentials;
	const handleSubmit = async (event) => {
		event.preventDefault();
		const { email, password } = credentials;
		// const { emailSignInStart } = this.props;
		emailSignInStart(email, password);
	};

	const handleChange = (event) => {
		const { name, value } = event.target;
		setCredentials({ ...credentials, [name]: value });
	};
	// render() {
	// const { googleSignInStart } = this.props;
	return (
		<div className='sign-in'>
			<h2>I Already Have An Account</h2>
			<span>Sign In With your Email And Password</span>
			<form onSubmit={handleSubmit}>
				<FormInput
					name='email'
					type='email'
					value={email}
					handleChange={handleChange}
					label='email'
					required
				/>
				<FormInput
					name='password'
					type='password'
					value={password}
					handleChange={handleChange}
					label='password'
					required
				/>
				<div className='buttons'>
					<CustomButton type='submit'>Sign In</CustomButton>
					<CustomButton
						type='button'
						onClick={googleSignInStart}
						isGoogleSignIn>
						Sign In With Google
					</CustomButton>
				</div>
			</form>
		</div>
	);
};
// }
const mapDispatchToProps = (dispatch) => ({
	googleSignInStart: () => dispatch(googleSignInStart()),
	emailSignInStart: (email, password) =>
		dispatch(emailSignInStart({ email, password }))
});
export default connect(null, mapDispatchToProps)(SignIn);
