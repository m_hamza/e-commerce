import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';
// import { response } from 'express';
const StripeCheckoutButton = ({ price }) => {
	const priceForStripe = price * 100;
	const publishableKey = 'pk_test_iKtwyUeUh9eBoK6edlaMf8v8005fwVXgzR';
	const onToken = (token) => {
		// alert('Payment Successful ');
		// console.log('Stripe Token', token);
		axios({
			url: 'payment',
			method: 'post',
			data: {
				amount: priceForStripe,
				token
			}
		})
			.then((response) => {
				alert('payment sucessful');
			})
			.catch((error) => {
				console.log('payment error:', JSON.parse(error));
				alert('payment Unsucessful');
			});
	};
	return (
		<StripeCheckout
			label='Pay Now'
			name='e-commerce Ltd.'
			billingAddress
			shippingAddress
			image='https://svgshare.com/i/CUz.svg'
			description={`Your Total is $${price}`}
			amount={priceForStripe}
			panelLabel='Pay Now'
			token={onToken}
			stripeKey={publishableKey}
		/>
	);
};
export default StripeCheckoutButton;
