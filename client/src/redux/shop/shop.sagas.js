import { takeLatest, call, put, all } from 'redux-saga/effects';
import ShopActionTypes from './shop.types';
import {
	convertCollectionsSnapshotToMap,
	firestore
} from '../../firebase/firebase.utils';
import { FetchCollectionsError, FetchCollectionsSuccess } from './shop.actions';

export function* fetchCollectionsAsync() {
	yield console.log('I was fired');
	try {
		const collectionRef = firestore.collection('collections');
		const snapshot = yield collectionRef.get();
		const collectionsMap = yield call(
			convertCollectionsSnapshotToMap,
			snapshot
		);
		yield put(FetchCollectionsSuccess(collectionsMap));
	} catch (error) {
		yield put(FetchCollectionsError(error.message));
	}
}
export function* fetchCollectionsStart() {
	yield takeLatest(
		ShopActionTypes.FETCH_COLLECTIONS_START,
		fetchCollectionsAsync
	);
}
export function* shopSagas() {
	yield all([call(fetchCollectionsStart)]);
}
