import ShopActionTypes from './shop.types';
import {
	firestore,
	convertCollectionsSnapshotToMap
} from '../../firebase/firebase.utils';
// export const updateCollections = (collectionsMap) => ({
// 	type: ShopActionTypes.UPDATE_COLLECTIONS,
// 	payload: collectionsMap
// });

export const FetchCollectionsStart = (collectionsMap) => ({
	type: ShopActionTypes.FETCH_COLLECTIONS_START
});

export const FetchCollectionsSuccess = (collectionsMap) => ({
	type: ShopActionTypes.FETCH_COLLECTIONS_SUCCESS,
	payload: collectionsMap
});

export const FetchCollectionsError = (errorMessage) => ({
	type: ShopActionTypes.FETCH_COLLECTIONS_FAILURE,
	payload: errorMessage
});

export const fetchCollectionsStartAsync = () => {
	return (dispatch) => {
		const collectionRef = firestore.collection('collections');
		dispatch(FetchCollectionsStart());
		collectionRef.get().then((snapshot) => {
			const collectionsMap = convertCollectionsSnapshotToMap(snapshot);
			dispatch(FetchCollectionsSuccess(collectionsMap));
		});
		// .catch((error) => {
		// 	dispatch(FetchCollectionsError(error.message));
		// });
	};
};
