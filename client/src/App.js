import React, { useEffect } from 'react';
import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom';

import Homepage from './pages/homepage/homepage.component';
import ShopPage from './pages/shop/shop.component';
import CheckoutPage from './pages/checkout/checkout.component';
import SignInAndSignUpPage from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component';
import Header from './components/header/header.component';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from './redux/user/user.selectors';
import { checkUserSession } from './redux/user/users.actions';
const App = ({ checkUserSession, currentUser }) => {
	// constructor() {
	// 	super();
	// 	this.state = {
	// 		currentUser: null
	// 	};
	// }
	// unsubscribeFromAuth = null;
	useEffect(() => {
		checkUserSession();
	}, [checkUserSession]);
	// componentDidMount() {
	// 	const { checkUserSession } = this.props;
	// 	checkUserSession();
	// const { setCurrentUser } = this.props;
	// this.unsubscribeFromAuth = auth.onAuthStateChanged(async (userAuth) => {
	// 	if (userAuth) {
	// 		const userRef = await createUserProfileDocument(userAuth);
	// 		userRef.onSnapshot((snapShot) => {
	// 			/*console.log("snapshot", snapShot);
	// 			this.setState(
	// 				{
	// 					currentUser: {
	// 						id: snapShot.id,
	// 						...snapShot.data()
	// 					}
	// 				},
	// 				() => {
	// 					console.log(this.state);
	// 				}
	// 			);*/
	// 			setCurrentUser({
	// 				id: snapShot.id,
	// 				...snapShot.data()
	// 			});
	// 		});
	// 	} else {
	// 		// this.setState({ currentUser: userAuth });
	// 		setCurrentUser(userAuth);
	// 	}
	// });
	// }

	// const componentWillUnmount() {
	// 	// this.unsubscribeFromAuth();
	// }

	// render() {
	// console.log(this.state.currentUser);
	return (
		<div>
			<Header /*currentUser={this.state.currentUser}*/ />
			<Switch>
				<Route exact path='/' component={Homepage} />
				<Route path='/shop' component={ShopPage} />
				<Route exact path='/checkout' component={CheckoutPage} />
				<Route
					path='/SignIn'
					render={() =>
						currentUser ? (
							<Redirect to='/' />
						) : (
							<SignInAndSignUpPage />
						)
					}
				/>
			</Switch>
		</div>
	);
	// }
};

const mapStateToProps = createStructuredSelector({
	currentUser: selectCurrentUser
});
const mapDispatchToProps = (dispatch) => ({
	checkUserSession: () => dispatch(checkUserSession())
});
// export default App;
export default connect(mapStateToProps, mapDispatchToProps)(App);
