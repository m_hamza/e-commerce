import React, {useEffect} from 'react';

import CollectionsOverviewContainer from '../../components/collection-overview/collection-overview.container';

import CollectionPageContainer from '../collection/collection.container';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import { FetchCollectionsStart } from '../../redux/shop/shop.actions';

const ShopPage = ({ match, fetchCollectionsStart }) => {
	// componentDidMount() {
	// 	const { fetchCollectionsStart } = this.props;
	// 	fetchCollectionsStart();
	// }
	useEffect(() => {
		fetchCollectionsStart()
	}, [fetchCollectionsStart]);
	// render() {
		// const { match } = this.props;
		return (
			<div className='shop-page'>
				<Route
					exact
					path={`${match.path}`}
					component={CollectionsOverviewContainer}
				/>
				<Route
					path={`${match.path}/:collectionId`}
					component={CollectionPageContainer}
				/>
			</div>
		);
	// }
}

const mapDispatchToProps = (dispatch) => ({
	fetchCollectionsStart: () => dispatch(FetchCollectionsStart())
});
export default connect(null, mapDispatchToProps)(ShopPage);
